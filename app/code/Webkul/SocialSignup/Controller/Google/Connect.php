<?php
/**
 * @category   Webkul
 * @package    Webkul_SocialSignup
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */ 
namespace Webkul\SocialSignup\Controller\Google;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Session\Generic;
use Magento\Store\Model\Store;
use Magento\Framework\Url;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Webkul\SocialSignup\Helper\Google;
use Webkul\SocialSignup\Helper\Data;

/**
 *Connect class of google
 */
class Connect extends Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
    /**
     * @var Store
     */
    protected $_store;
    /**
     * @var \Magento\Framework\Session\Generic
     */
    protected $_session;
    /**
     * @var Url
     */
    protected $_url;
    /**
     * @var Attribute
     */
    protected $_eavAttribute;
    /**
     * @var Google
     */
    protected $_helperGoogle;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    
    /**
     * @param Generic                                            $session
     * @param Context                                            $context
     * @param Store                                              $store
     * @param Data                                               $helper
     * @param Google                                             $helperGoogle
     * @param Attribute                                          $eavAttribute
     * @param \Magento\Framework\UrlInterface                    $urlinterface
     * @param GoogleClient                                       $googleClient
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Customer\Model\Session                    $customerSession
     * @param \Magento\Framework\Controller\ResultInterface      $result
     * @param PageFactory                                        $resultPageFactory
     */
    public function __construct(
        Data $helper,
        Generic $session,
        Context $context,
        Store $store,
        Google $helperGoogle,
        Attribute $eavAttribute,
        GoogleClient $googleClient,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Session $customerSession,
        PageFactory $resultPageFactory
    ) {
        $this->_helper = $helper;
        $this->_customerSession = $customerSession;
        $this->_helperGoogle = $helperGoogle;
        $this->_eavAttribute = $eavAttribute;
        $this->_store = $store;
        $this->_scopeConfig = $scopeConfig;
        $this->_session = $session;
        $this->googleClient = $googleClient;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * generate access token and user information
     */
    public function execute()
    {
        $this->googleClient->setParameters();
        $helper = $this->_objectManager->get('Webkul\SocialSignup\Helper\Data');
        try {
                $isSecure = $this->_store->isCurrentlySecure();
                $mainwProtocol = $this->_session->getIsSecure();
                $this->_connectCallback();
        } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
        }

        if (!empty($this->referer)) {
            if (empty($this->flag)) {
                if (!$isSecure) {
                    $redirectUrl = $this->_url->getUrl('socialsignup/google/redirect/');
                    $redirectUrl = str_replace("https://", "http://", $redirectUrl);
                    $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    return $resultRedirect->setPath($redirectUrl);
                } else {
                    $this->_helper->_loginFinalize($this);
                }
            } else {
                $this->_helper->closeWindow($this);
            }
        } else {
             $helper->redirect404($this);
        }
    }

    protected function _connectCallback()
    {
        $errorCode = $this->getRequest()->getParam('error');
        $code = $this->getRequest()->getParam('code');
        $state = $this->getRequest()->getParam('state');
        if (!($errorCode || $code) && !$state) {
            // Direct route access - deny
            return;
        }
        
        $this->referer = $this->_url->getCurrentUrl();

        if (!$state || $state != $this->_session->getGoogleCsrf()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Unable to find google Csrf Code')
            );
        }

        if ($errorCode) {
            // Google API read light - abort
            if ($errorCode === 'access_denied') {
                unset($this->referer);
                $this->flag = "noaccess";
                $this->_helper->closeWindow($this);
            }
            return;
        }

        if ($code) {
            $attributegId = $this->_eavAttribute->getIdByCode('customer', 'socialsignup_gid');
            $attributegtoken = $this->_eavAttribute->getIdByCode('customer', 'socialsignup_gtoken');
            if ($attributegId == false || $attributegtoken == false) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Attribute socialsignup_gid or socialsignup_gtoken not exist')
                );
            }
            // Google API green light - proceed
            $userInfo = $this->googleClient->api('/userinfo');

            $token = $this->googleClient->getAccessToken();
            
            $customersByGoogleId = $this->_helperGoogle
                ->getCustomersByGoogleId($userInfo->id);

            $this->_connectWithActiveAccount($customersByGoogleId, $userInfo, $token);

            $this->_checkAccountByGoogleId($customersByGoogleId);

            $customersByEmail = $this->_helperGoogle
                ->getCustomersByEmail($userInfo->email);

            if ($customersByEmail->count()) {
                // Email account already exists - attach, login
                $this->_helperGoogle->connectByGoogleId(
                    $customersByEmail,
                    $userInfo->id,
                    $token
                );

                $this->messageManager->addSuccess(
                    __(
                        'We have discovered you already have an account at our store.
                        Your %1 account is now connected to your store account.',
                        __(
                            'Google'
                        )
                    )
                );

                return;
            }

            // New connection - create, attach, login
            if (empty($userInfo->given_name)) {
                $this->messageManager->addError(
                    __('Sorry, could not retrieve your %1 first name. Please try again.', __('Google'))
                );
            }

            if (empty($userInfo->family_name)) {
                $this->messageManager->addError(
                    __('Sorry, could not retrieve your %2 last name. Please try again.', __('Google'))
                );
            }

            $this->_helperGoogle->connectByCreatingAccount(
                $userInfo->email,
                $userInfo->given_name,
                $userInfo->family_name,
                $userInfo->id,
                $token
            );

            $this->messageManager->addSuccess(
                __(
                    'Your %1 account is now connected to your new user account at our store.
                    Now you can login using our %1 Connect button or using store account 
                    credentials you will receive to your email address.',
                    __(
                        'Google'
                    )
                )
            );
        }
    }

    /**
     * connected with existing account
     * @param  object $customersByGoogleId
     * @param  object $userInfo
     * @param  string $token
     */
    private function _connectWithActiveAccount($customersByGoogleId, $userInfo, $token)
    {
        if ($this->_customerSession->isLoggedIn()) {
            // Logged in user
            if ($customersByGoogleId->count()) {
                // Google account already connected to other account - deny
                $this->messageManager
                    ->addNotice(
                        __(
                            'Your %1 account is already connected to one of our store accounts.',
                            __(
                                'Google'
                            )
                        )
                    );

                return;
            }

            // Connect from account dashboard - attach
            $customer = $this->_customerSession->getCustomer();

            $this->_helperGoogle->connectByGoogleId(
                $customer,
                $userInfo->id,
                $token
            );

            $this->messageManager->addSuccess(
                __(
                    'Your %1 account is now connected to your store account.
                    You can now login using our %1 Connect button or using 
                    store account credentials you will receive to your email address.',
                    __(
                        'Google'
                    )
                )
            );
            return;
        }
    }

    /**
     * check customer account by google id
     * @param  object $customersByGoogleId
     */
    private function _checkAccountByGoogleId($customersByGoogleId)
    {
        if ($customersByGoogleId->count()) {
            // Existing connected user - login
            foreach ($customersByGoogleId as $customerInfo) {
                $customer = $customerInfo;
            }

            $this->_helperGoogle->loginByCustomer($customer);

            $this->messageManager
                ->addSuccess(
                    __('You have successfully logged in using your %1 account.', __('Google'))
                );
            return;
        }
    }
}
