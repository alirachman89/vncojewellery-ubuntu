<?php
/**
 * @category   Webkul
 * @package    Webkul_SocialSignup
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */ 
namespace Webkul\SocialSignup\Controller\Linkedin;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Session\Generic;
use Magento\Store\Model\Store;
use Magento\Framework\Url;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Webkul\SocialSignup\Helper\Linkedin;
use Magento\Framework\Exception\LocalizedException;

class Connect extends Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
    /**
     * @var helperLinkedin
     */
    protected $_helperLinkedin;
    /**
     * @var eavAttribute
     */
    protected $_eavAttribute;
    /**
     * @var store
     */
    protected $_store;
    /**
     * @var scopeConfig
     */
    protected $_scopeConfig;
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Generic $session,
        Context $context,
        Store $store,
        Linkedin $helperLinkedin,
        Attribute $eavAttribute,
        LinkedinClient $linkedinClient,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\SocialSignup\Helper\Data $helper,
        PageFactory $resultPageFactory
    ) {
    
        $this->customerSession = $customerSession;
        $this->_helperLinkedin = $helperLinkedin;
        $this->_eavAttribute = $eavAttribute;
        $this->_store = $store;
        $this->_scopeConfig = $scopeConfig;
        $this->_session = $session;
        $this->_helper = $helper;
        $this->_linkedinClient = $linkedinClient;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $this->_linkedinClient->setParameters();
            $isSecure = $this->_store->isCurrentlySecure();
            $mainwProtocol = $this->_session->getIsSecure();
            $this->_connectCallback();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

        if (!empty($this->referer)) {
            if (empty($this->flag)) {
                if (!$isSecure) {
                    $redirectUrl = $this->_url->getUrl('socialsignup/linkedin/redirect/');
                    $redirectUrl = str_replace("https://", "http://", $redirectUrl);
                    $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    return $resultRedirect->setPath($redirectUrl);
                } else {
                    $this->_helper->_loginFinalize($this);
                }
            } else {
                $this->_helper->closeWindow($this);
            }
        } else {
            $helper->redirect404($this);
        }
    }
    protected function _connectCallback()
    {
        $errorCode = $this->getRequest()->getParam('error');
        $code = $this->getRequest()->getParam('code');
        $state = $this->getRequest()->getParam('state');
        if (!($errorCode || $code) && !$state) {
            return;
        }
        $this->referer = $this->_url->getCurrentUrl();

        if ($errorCode) {
            if ($errorCode === 'access_denied') {
                unset($this->referer);
                $this->flag = "noaccess";
                $this->_helper->closeWindow($this);
            }
            return;
        }
        if ($code) {
            $attributegId = $this->_eavAttribute->getIdByCode('customer', 'socialsignup_lid');
            $attributegtoken = $this->_eavAttribute->getIdByCode('customer', 'socialsignup_ltoken');
            if ($attributegId == false || $attributegtoken == false) {
                throw new  \Magento\Framework\Exception\LocalizedException(
                    __('Attribute `socialsignup_lid` or `socialsignup_ltoken` not exist')
                );
            }
            $token = $this->_linkedinClient->getAccessToken();

            $userInfo = $this->_linkedinClient->api('/v1/people/~');
            
            $customersByLinkedinId = $this->_helperLinkedin
                ->getCustomersByLinkedinId($userInfo->id);

            $this->_connectWithCurrentCustomer($customersByLinkedinId, $userInfo, $token);

            if ($customersByLinkedinId->count()) {
                foreach ($customersByLinkedinId as $key => $customerInfo) {
                    $customer = $customerInfo;
                }
                $this->_helperLinkedin->loginByCustomer($customer);

                $this->messageManager
                    ->addSuccess(
                        __('You have successfully logged in using your %1 account.', __('LinkedIn'))
                    );

                return;
            }

            $customersByEmail = $this->_helperLinkedin
                ->getCustomersByEmail($userInfo->emailAddress);

            if ($customersByEmail->count()) {
                $this->_helperLinkedin->connectByLinkedinId(
                    $customersByEmail,
                    $userInfo->id,
                    $token
                );

                $this->messageManager->addSuccess(
                    __(
                        'We have discovered you already have an account at our store.
                         Your %1 account is now connected to your store account.',
                        __(
                            'LinkedIn'
                        )
                    )
                );

                return;
            }

            if (empty($userInfo->firstName)) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Sorry, could not retrieve your %1 first name. Please try again.', __('LinkedIn'))
                );
            }

            if (empty($userInfo->lastName)) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Sorry, could not retrieve your %1 last name. Please try again.', __('LinkedIn'))
                );
            }

            $this->_helperLinkedin->connectByCreatingAccount(
                $userInfo->emailAddress,
                $userInfo->firstName,
                $userInfo->lastName,
                $userInfo->id,
                $token
            );

            $this->messageManager->addSuccess(
                __(
                    'Your %1 account is now connected to your new user account at our store.
                    Now you can login using our %1 Connect button or using store account credentials
                    you will receive to your email address.',
                    __(
                        'LinkedIn'
                    )
                )
            );
        }
    }

    private function _connectWithCurrentCustomer($customersByLinkedinId, $userInfo, $token)
    {
        if ($this->customerSession->isLoggedIn()) {
            if ($customersByLinkedinId->count()) {
                $this->messageManager
                    ->addNotice(
                        __('Your %1 account is already connected to one of our store accounts.', __('LinkedIn'))
                    );

                return;
            }

            $customer = $this->customerSession->getCustomer();

            $this->_helperLinkedin->connectByLinkedinId(
                $customer,
                $userInfo->id,
                $token
            );

            $this->messageManager->addSuccess(
                __(
                    'Your %1 account is now connected to your store account.
                    You can now login using our %1 Connect button or using store
                    account credentials you will receive to your email address.',
                    __(
                        'Linkedin'
                    )
                )
            );

            return;
        }
    }
}
