<?php
/**
 * @category   Webkul
 * @package    Webkul_SocialSignup
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */ 
namespace Webkul\SocialSignup\Helper;

use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Framework\View\Result\PageFactory;

/**
 * Social Signup data helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const CHARS_LOWERS                          = 'abcdefghijklmnopqrstuvwxyz';
    const CHARS_UPPERS                          = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const CHARS_DIGITS                          = '0123456789';
    const CHARS_SPECIALS                        = '!$*+-.=?@^_|~';
    const CHARS_PASSWORD_LOWERS                 = 'abcdefghjkmnpqrstuvwxyz';
    const CHARS_PASSWORD_UPPERS                 = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
    const CHARS_PASSWORD_DIGITS                 = '23456789';
    const CHARS_PASSWORD_SPECIALS               = '!$*-.=?@_';
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param Imagelink                                                               $imageLink
     * @param \Magento\Framework\App\Helper\Context                                   $context
     * @param \Magento\Framework\ObjectManagerInterface                               $objectManager
     * @param \Magento\Customer\Model\Session                                         $customerSession
     * @param \Magento\Store\Model\StoreManagerInterface                              $storeManager
     */
    public function __construct(
        Imagelink $imageLink,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $collectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        PageFactory $resultPageFactory
    ) {
        $this ->_imageLink = $imageLink;
        $this->_objectManager = $objectManager;
        $this->_customerSession = $customerSession;
        $this->collectionFactory = $collectionFactory;
        $this->_scopeConfig = $context->getScopeConfig();
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->_resultPageFactory = $resultPageFactory;
    }

    /**
     * get facebook app id
     * @return string
     */
    public function getFbAppId()
    {
        return $this->_scopeConfig->getValue(
            'socialsignup/fblogin/appid',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get facebook app secret key
     * @return string
     */
    public function getFbSecretKey()
    {
         return $this->_scopeConfig->getValue(
             'socialsignup/fblogin/secret',
             \Magento\Store\Model\ScopeInterface::SCOPE_STORE
         );
    }

    /**
     * get Google client id
     * @return string
     */
    public function getGoogleClientId()
    {
        return $this->_scopeConfig->getValue(
            'socialsignup/google/api_key',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get Google secret key
     * @return string
     */
    public function getGoogleSecretKey()
    {
        return $this->_scopeConfig->getValue(
            'socialsignup/google/secret',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get LinkedIn app id
     * @return string
     */
    public function getLinkedinAppId()
    {
        return $this->_scopeConfig->getValue(
            'socialsignup/linkedin/api_key',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get linkedin secret key
     * @return int
     */
    public function getLinkedinSecret()
    {
        return $this->_scopeConfig->getValue(
            'socialsignup/linkedin/secret',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get cosumer key of twitter
     * @return integer
     */
    public function getConsumerKey()
    {
        return $this->_scopeConfig->getValue(
            'socialsignup/twitterlogin/consumerkey',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get secrete key of twitter
     * @return string
     */
    public function getConsumerSecret()
    {
        return $this->_scopeConfig->getValue(
            'socialsignup/twitterlogin/consumersecret',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get application key of instagram
     * @return integer
     */
    public function getInstaClientId()
    {
        return $this->_scopeConfig->getValue(
            'socialsignup/instagram/api_key',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get secrete key of instagram
     * @return string
     */
    public function getInstaSecretKey()
    {
        return $this->_scopeConfig->getValue(
            'socialsignup/instagram/secret',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * check the status of facebook
     * @return boolean
     */
    public function getFbStatus()
    {
        return $this->_scopeConfig->getValue(
            'socialsignup/fblogin/enabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * check the status of google
     * @return boolean
     */
    public function getGoogleStatus()
    {
        return $this->_scopeConfig->getValue(
            'socialsignup/google/enabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * check the status of linkedIn
     * @return boolean
     */
    public function getLinkedInStatus()
    {
        return $this->_scopeConfig->getValue(
            'socialsignup/linkedin/enabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * check the status of twitter
     * @return boolean
     */
    public function getTwitterStatus()
    {
        return $this->_scopeConfig->getValue(
            'socialsignup/twitterlogin/enabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * check the status of instgram
     * @return boolean
     */
    public function getInstaStatus()
    {
        return $this->_scopeConfig->getValue(
            'socialsignup/instagram/enabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get Customer session
     * @return boolean
     */
    public function customerSession()
    {
        return $this->_customerSession->isLoggedIn();
    }

    /**
     * get facbook image
     * @return string
     */
    public function getLoginImg()
    {
        $img = $this->_scopeConfig->getValue(
            'socialsignup/fblogin/imglogin',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if (empty($img)) {
             $img = $this ->_imageLink->getWebImageLink('Webkul_SocialSignup::images/icon-facebook.png');
        } else {
            $img = $this->_storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            ).'socialsignup/fb/' . $img;
        }
        return $img;
    }

    /**
     * get twitter image
     * @return string
     */
    public function getTwitterLoginImg()
    {
        $img = $this->_scopeConfig->getValue(
            'socialsignup/twitterlogin/imglogin',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if (empty($img)) {
            $img = $this ->_imageLink->getWebImageLink('Webkul_SocialSignup::images/icon-twitter.png');
        } else {
            $img = $this->_storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            ).'socialsignup/twitter/' . $img;
        }
        return $img;
    }

    /**
     * get google login image
     * @return string
     */
    public function getGoogleLoginImg()
    {
        $img = $this->_scopeConfig->getValue(
            'socialsignup/google/imglogin',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if (empty($img)) {
            $img = $this ->_imageLink->getWebImageLink('Webkul_SocialSignup::images/icon-google-plus.png');
        } else {
            $img = $this->_storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            ).'socialsignup/google/' . $img;
        }
        return $img;
    }

    /**
     * get LinkedIn login image
     * @return string
     */
    public function getLinkedinLoginImg()
    {
        $img = $this->_scopeConfig->getValue(
            'socialsignup/linkedin/imglogin',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if (empty($img)) {
            $img = $this ->_imageLink->getWebImageLink('Webkul_SocialSignup::images/icon-linkedin.png');
        } else {
            $img = $this->_storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            ).'socialsignup/linkedin/' . $img;
        }
        return $img;
    }

    /**
     * get instagram login image
     * @return string
     */
    public function getInstaLoginImg()
    {
        $img = $this->_scopeConfig->getValue(
            'socialsignup/instagram/imglogin',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if (empty($img)) {
            $img = $this ->_imageLink->getWebImageLink('Webkul_SocialSignup::images/icon-Instagram.png');
        } else {
            $img = $this->_storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            ).'socialsignup/instagram/' . $img;
        }
        return $img;
    }

    /**
     * redirect to 404 cms page
     * @param  Action $frontController object of controller
     * @return redirect to url
     */
    public function redirect404(Action $frontController)
    {
        $pageHelper = $this->_objectManager->get('Magento\Cms\Helper\Page');
        $pageId = $this->_scopeConfig->getValue('web/default/cms_no_route');
        $resultPage = $pageHelper->prepareResultPage($frontController, $pageId);
        if ($resultPage) {
            $resultPage->setStatusHeader(404, '1.1', 'Not Found');
            $resultPage->setHeader('Status', '404 File not found');
            return $resultPage;
        }
    }

    /**
     * getnerate password
     * @param  integer $length length of password
     * @return string
     */
    public function generatePassword($length = 8)
    {
        $chars = self::CHARS_PASSWORD_LOWERS
            . self::CHARS_PASSWORD_UPPERS
            . self::CHARS_PASSWORD_DIGITS
            . self::CHARS_PASSWORD_SPECIALS;
         return   $this->getRandomString($length, $chars);
    }

    /**
     * generate password with ramdom string
     * @param  integer $len   length of password
     * @param  string $chars string of charcters
     * @return string
     */
    protected function getRandomString($len, $chars = null)
    {
        if ($chars==null) {
            $chars = self::CHARS_LOWERS . self::CHARS_UPPERS . self::CHARS_DIGITS;
        }
        for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++) {
            $str .= $chars[mt_rand(0, $lc)];
        }
        return $str;
    }

    /**
     * refresh the page
     */
    public function _loginFinalize($objectAction)
    {
        $resultPage = $this->_resultPageFactory->create();
        $block = $resultPage->getLayout()
                ->createBlock('\Magento\Framework\View\Element\Template')
                ->setTemplate('Webkul_SocialSignup::socialsignup/window-close.phtml')
                ->toHtml();
        $objectAction->getResponse()->setBody($block);
    }

    /**
     * closed popup
     */
    public function closeWindow($objectAction)
    {
        $resultPage = $this->_resultPageFactory->create();
        $block = $resultPage->getLayout()
                ->createBlock('\Magento\Framework\View\Element\Template')
                ->setTemplate('Webkul_SocialSignup::socialsignup/window-close.phtml')
                ->toHtml();
        $objectAction->getResponse()->setBody($block);
    }
}
