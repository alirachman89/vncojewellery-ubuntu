<?php
namespace Emizentech\CustomSmtp\Model\Transport;

/**
 * Interceptor class for @see \Emizentech\CustomSmtp\Model\Transport
 */
class Interceptor extends \Emizentech\CustomSmtp\Model\Transport implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Mail\MessageInterface $message, \Emizentech\CustomSmtp\Helper\Data $helper)
    {
        $this->___init();
        parent::__construct($message, $helper);
    }

    /**
     * {@inheritdoc}
     */
    public function sendMessage()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'sendMessage');
        if (!$pluginInfo) {
            return parent::sendMessage();
        } else {
            return $this->___callPlugins('sendMessage', func_get_args(), $pluginInfo);
        }
    }
}
