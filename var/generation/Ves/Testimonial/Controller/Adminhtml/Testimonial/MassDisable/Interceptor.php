<?php
namespace Ves\Testimonial\Controller\Adminhtml\Testimonial\MassDisable;

/**
 * Interceptor class for @see \Ves\Testimonial\Controller\Adminhtml\Testimonial\MassDisable
 */
class Interceptor extends \Ves\Testimonial\Controller\Adminhtml\Testimonial\MassDisable implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Ui\Component\MassAction\Filter $filter, \Ves\Testimonial\Model\ResourceModel\Testimonial\CollectionFactory $collectionFactory)
    {
        $this->___init();
        parent::__construct($context, $filter, $collectionFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
