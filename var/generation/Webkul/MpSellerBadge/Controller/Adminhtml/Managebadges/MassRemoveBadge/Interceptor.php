<?php
namespace Webkul\MpSellerBadge\Controller\Adminhtml\Managebadges\MassRemoveBadge;

/**
 * Interceptor class for @see \Webkul\MpSellerBadge\Controller\Adminhtml\Managebadges\MassRemoveBadge
 */
class Interceptor extends \Webkul\MpSellerBadge\Controller\Adminhtml\Managebadges\MassRemoveBadge implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Webkul\Marketplace\Model\Seller $sellerModel, \Magento\Ui\Component\MassAction\Filter $filter, \Webkul\MpSellerBadge\Api\SellerbadgeRepositoryInterface $sellerBadgeFactory, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->___init();
        parent::__construct($context, $sellerModel, $filter, $sellerBadgeFactory, $resultPageFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
