<?php
namespace Webkul\SocialSignup\Controller\Twitter\Request;

/**
 * Interceptor class for @see \Webkul\SocialSignup\Controller\Twitter\Request
 */
class Interceptor extends \Webkul\SocialSignup\Controller\Twitter\Request implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Webkul\SocialSignup\Helper\Data $data, \Magento\Framework\Session\Generic $session, \Webkul\SocialSignup\Controller\Twitter\TwitterClient $twitterClient, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->___init();
        parent::__construct($context, $data, $session, $twitterClient, $resultPageFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
