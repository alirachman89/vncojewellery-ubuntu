<?php
namespace Webkul\SocialSignup\Controller\Linkedin\Connect;

/**
 * Interceptor class for @see \Webkul\SocialSignup\Controller\Linkedin\Connect
 */
class Interceptor extends \Webkul\SocialSignup\Controller\Linkedin\Connect implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Session\Generic $session, \Magento\Framework\App\Action\Context $context, \Magento\Store\Model\Store $store, \Webkul\SocialSignup\Helper\Linkedin $helperLinkedin, \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute, \Webkul\SocialSignup\Controller\Linkedin\LinkedinClient $linkedinClient, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Customer\Model\Session $customerSession, \Webkul\SocialSignup\Helper\Data $helper, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->___init();
        parent::__construct($session, $context, $store, $helperLinkedin, $eavAttribute, $linkedinClient, $scopeConfig, $customerSession, $helper, $resultPageFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
